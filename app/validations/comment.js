const Joi = require('joi');

module.exports = {
  create: {
    texte: Joi.string().required(),
    think: Joi.string().required(),
    createdBy: Joi.string().required(),
    updatedBy: Joi.string().optional(),
  },
  update: {
    texte: Joi.string().optional(),
    think: Joi.string().optional(),
    createdBy: Joi.string().optional(),
    updatedBy: Joi.string().optional(),
  },
  remove: {
    id: Joi.string().required()
  }
}