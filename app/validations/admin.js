const Joi = require('joi');

module.exports = {
  register: {
    email: Joi.string().required(),
    password: Joi.string().required(),
    firstname: Joi.string().optional(),
    lastname: Joi.string().optional(),
    role: Joi.string().required(),
    pseudo: Joi.string().optional(),
    
  },
  login: {
    email: Joi.string().required(),
    password: Joi.string().required(),
  },
  update: {
    email: Joi.string().optional(),
    password: Joi.string().optional(),
    firstname: Joi.string().optional(),
    lastname: Joi.string().optional(),
    role: Joi.string().optional(),
    pseudo: Joi.string().optional(),
  },
  remove: {
    id: Joi.string().required()
  }
}