const admin = require('./admin');
const comment = require('./comment');
const think = require('./think');

module.exports = {
  admin,
  comment,
  think
};