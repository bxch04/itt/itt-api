const Joi = require('joi');

module.exports = {
  create: {
    author: Joi.string().optional(),
    texte: Joi.string().required(),
    ref: Joi.string().optional(),
    like: Joi.string().optional(),
    share: Joi.string().optional(),
    favorite: Joi.string().optional(),
    nbrcom: Joi.string().optional(),
    createdBy: Joi.string().required(),
    updatedBy: Joi.string().optional(),
  },
  update: {
    author: Joi.string().optional(),
    texte: Joi.string().optional(),
    ref: Joi.string().optional(),
    like: Joi.string().optional(),
    share: Joi.string().optional(),
    favorite: Joi.string().optional(),
    nbrcom: Joi.string().optional(),
    createdBy: Joi.string().optional(),
    updatedBy: Joi.string().optional(),
  },
  remove: {
    id: Joi.string().required()
  }
}