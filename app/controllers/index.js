const UserController = require('./user');
const ResourceController = require('./resource');
const validation = require('../validations');

module.exports = {
  admin: new UserController('admin', 'admins', validation.admin),

  comment: new ResourceController('comment', 'comments', validation.comment),
  think: new ResourceController('think', 'thinks', validation.think),
};