'use strict';
const MainHelper = require('../helpers');
const UtilHelper = require('../helpers/utils');
const S3Helper = require('../helpers/s3');

module.exports = class ResourceController {
  constructor (type, plural, validate) {
    this.type = type;
    this.plural = plural;
    this.validate = validate;
  }

  get () {
    return {
      description: 'Returns the ' + this.type + ' info',
      auth: 'jwt',
      handler: async (request, h) => {
        try {
          return h.response({
            [this.type]: await MainHelper[this.type].get(request.params.resource)
          }).code(200);
        } catch (error) {
          return error.message;
        }
      },
      tags: ['api']
    };
  }

  count () {
    return {
      description: 'Returns the count of ' + this.plural,
      auth: 'jwt',
      handler: async (request, h) => {
        try { 
          if (request.query.createdAt) {
            request.query.createdAt = JSON.parse(request.query.createdAt);
          }
        
          return h.response({
            [this.plural]: await MainHelper[this.type].count(request.query)
          }).code(200);
        } catch (error) {
          return error.message;
        }
      },
      tags: ['api']
    };
  } 
  
  listAuth () {
    return {
      description: 'Returns the list of ' + this.total,
      auth: 'jwt',
      handler: async (request, h) => {
        try {

          let query = request.query.options ? JSON.parse(request.query.options) : {};
          let filter = {};
          let sort = {};

          if (request.query.filter) {
            const filterData = JSON.parse(request.query.filter);
            if (filterData.text) {
              filter = {$or: []};

              filterData.type.forEach(type => {
                filter['$or'].push({[type]: new RegExp(filterData.text, 'gi')});
              });
            }
          }

          if (request.query.sort) {
            let sortData = JSON.parse(request.query.sort);
            for (var p in sortData) {
              if (sortData[p] !== 0) {     
                sort[p] = sortData[p];
              }
            }
          }

          const params = {
            ...query,
            ...filter
          };


          const page = request.query.page ? parseInt(request.query.page) : 0;
          const perPage = request.query.perPage ? parseInt(request.query.perPage) : 30;
          
          let resources = await MainHelper[this.type].list(params, page, perPage, sort);
          let resourceCount = await MainHelper[this.type].count(params);

          return h.response({
            [this.plural]: resources,
            total: resourceCount
          }).code(200);
        } catch (error) {
          return error.message;
        }
      },
      tags: ['api']
    };
  } 

  list () {
    return {
      description: 'Returns the list of ' + this.total,
      auth: false,
      handler: async (request, h) => {
        try {

          let query = request.query.options ? JSON.parse(request.query.options) : {};
          let filter = {};
          let sort = {};

          if (request.query.filter) {
            const filterData = JSON.parse(request.query.filter);
            if (filterData.text) {
              filter = {$or: []};

              filterData.type.forEach(type => {
                filter['$or'].push({[type]: new RegExp(filterData.text, 'gi')});
              });
            }
          }

          if (request.query.sort) {
            let sortData = JSON.parse(request.query.sort);
            for (var p in sortData) {
              if (sortData[p] !== 0) {     
                sort[p] = sortData[p];
              }
            }
          }

          const params = {
            ...query,
            ...filter
          };


          const page = request.query.page ? parseInt(request.query.page) : 0;
          const perPage = request.query.perPage ? parseInt(request.query.perPage) : 30;
          
          let resources = await MainHelper[this.type].list(params, page, perPage, sort);
          let resourceCount = await MainHelper[this.type].count(params);

          return h.response({
            [this.plural]: resources,
            total: resourceCount
          }).code(200);
        } catch (error) {
          return error.message;
        }
      },
      tags: ['api']
    };
  } 
  
  create () {
    return {
      description: 'Create a new ' + this.type,
      auth: 'jwt',
      validate: {
        payload: this.validate.create,
        failAction: (request, h, error) => {
          return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
        }
      },
      handler: async (request, h) => {
        try {
          let data = await MainHelper[this.type].create({...request.payload});
          return h.response({ message: data.message }).code(data.statusCode);
        } catch (error) {
          return error.message;
        }
      },
      tags: ['api']
    };
  }

  createWithoutAuth () {
    return {
      description: 'Create a new ' + this.type,
      auth: false,
      validate: {
        payload: this.validate.create,
        failAction: (request, h, error) => {
          return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
        }
      },
      handler: async (request, h) => {
        try {
          let data = await MainHelper[this.type].create({...request.payload});
          return h.response({ message: data.message }).code(data.statusCode);
        } catch (error) {
          return error.message;
        }
      },
      tags: ['api']
    };
  }

  createWithFile () {
    return {
      description: 'Create a new ' + this.type,
      auth: 'jwt',
      payload: {
        output: 'stream',
        allow: 'multipart/form-data', // important,
        maxBytes: 10 * 1024 * 1024
      },
      validate: {
        payload: this.validate.createWithFile,
        failAction: (request, h, error) => {
          return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
        }
      },
      handler: async (request, h) => { 
        try {
          let dataRaw = JSON.parse(request.payload.objectdata);

          dataRaw[request.payload.fileprop] = [];
          
          for (var i = 0; i < parseInt(request.payload.filenumber); i++) {
            if (request.payload['filedata' + i] && request.payload['filedata' + i] != 'null') {
              const file = await S3Helper.uploadImage(request.payload['filedata' + i]);
              dataRaw[request.payload.fileprop].push(file.location);
            }
          }

          let data = await MainHelper[this.type].create(dataRaw);
          
          return h.response({ message: data.message }).code(data.statusCode);
        } catch (error) {
          return error.message;
        }
      },
      tags: ['api']
    };
  }

  createOrUpdate () {
    return {
      description: 'Create a new set of ' + this.type,
      auth: 'jwt',
      validate: {
        payload: this.validate.createOrUpdate,
        failAction: (request, h, error) => {
          return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
        }
      },
      handler: async (request, h) => {
        try {
          let data = await MainHelper[this.type].createOrUpdate(request.payload.data, request.payload.filter);
          return h.response({ message: data.message }).code(data.statusCode);
        } catch (error) {
          console.log(error)
          return error.message;
        }
      },
      tags: ['api']
    };
  } 
  
  import () {
    return {
      description: 'Create a new ' + this.type,
      auth: 'jwt',
      payload: {
          output: 'stream',
          allow: 'multipart/form-data' // important
      },
      validate: {
        payload: this.validate.import,
        failAction: (request, h, error) => {
          return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
        }
      },
      handler: async (request, h) => {
        try {
          let data = UtilHelper.parseExcel(request.payload.file._data);
          const values = await this.validate.importData(data, request.payload.ecole);
          // console.log(values);
          // return 'yo';
          return await MainHelper[this.type].createOrUpdate(values);
        } catch (error) {
          console.error(error);
          return error.message;
        }
      },
      tags: ['api']
    };
  } 
  
  update () {
    return {
      description: 'Update the ' + this.type,
      auth: 'jwt',
      validate: {
        payload: this.validate.update,
        failAction: (request, h, error) => {
          return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
        }
      },
      handler: async (request, h) => {
        try {
          let resourceId = request.params.resource;
          let resourceData = request.payload;

          let data = await MainHelper[this.type].update(resourceId, resourceData);
    
          return h.response({ message: 'Done', [this.type]: data.resource }).code(200);
        } catch (error) {
          return error.message;
        }
      },
      tags: ['api']
    };
  }


  updateWithFile () {
    return {
      description: 'Update with file a new ' + this.type,
      auth: 'jwt',
      payload: {
        output: 'stream',
        allow: 'multipart/form-data' // important
      },
      validate: {
        payload: this.validate.createWithFile,
        failAction: (request, h, error) => {
          return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
        }
      },
      handler: async (request, h) => {
        try {
          let resourceId = request.params.resource;
          let dataRaw = JSON.parse(request.payload.objectdata);
          let fileLinks = JSON.parse(request.payload.filelinks);

          dataRaw[request.payload.fileprop] = [];

          for (var i = 0; i < parseInt(request.payload.filenumber); i++) {
            if (request.payload['filedata' + i] && request.payload['filedata' + i] != 'null') {
              const file = await S3Helper.uploadImage(request.payload['filedata' + i]);
              dataRaw[request.payload.fileprop].push(file.location);
            } else {
              dataRaw[request.payload.fileprop].push(fileLinks[i]);
            }
          }
          
          let data = await MainHelper[this.type].update(resourceId, dataRaw);
          return h.response({ message: data.message }).code(data.statusCode);
        } catch (error) {
          return error.message;
        }
      },
      tags: ['api']
    };
  }
  
  remove () {
    return {
      description: 'Remove the ' + this.type,
      auth: 'jwt',
      validate: {
        query: this.validate.remove,
        failAction: (request, h, error) => {
          return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
        }
      },
      handler: async (request) => {
        try {
          // console.log('My id :', request.query.id);
          return await MainHelper[this.type].remove(request.query.id);
        } catch (error) {
          return error.message;
        }
      },
      tags: ['api']
    };
  } 
};