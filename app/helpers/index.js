const UserHelper = require('./user');
const ResourceHelper = require('./resource');

module.exports = {
  
  admin: new UserHelper('Admin', 'email'),

  think: new ResourceHelper('Think', 'admin'),
  comment: new ResourceHelper('Comment', 'think'),

};