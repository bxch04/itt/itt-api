'use strict';
const MainController = require('../../controllers');

exports.plugin = {
  register: (plugin, options) => {
    const Modules = [
      {
        path: 'admins/',
        ctrl: MainController.admin,
      }
    ];

    const basePath = '/api/v1/';

    for (let i = 0; i < Modules.length; i++) {
      const mdl = Modules[i];  
      const modulePath = basePath + mdl.path;
      
      plugin.route([
        {
          method: 'GET',
          path: modulePath + "{user}",
          config: mdl.ctrl.get()
        },
        {
          method: 'GET',
          path: modulePath + 'me',
          config: mdl.ctrl.getDetails()
        },
        {
          method: 'GET',
          path: modulePath + 'list',
          config: mdl.ctrl.list()
        },
        {
          method: 'GET',
          path: modulePath + 'count',
          config: mdl.ctrl.count()
        },
        {
          method: 'POST',
          path: modulePath + 'login',
          config: mdl.ctrl.login()
        },
        {
          method: 'POST',
          path: modulePath + 'register',
          config: mdl.ctrl.register()
        },
        {
          method: 'PUT',
          path: modulePath + '{user}',
          config: mdl.ctrl.update()
        },
        { 
          method: 'DELETE',
          path: modulePath + 'remove',
          config: mdl.ctrl.remove()
        }
      ]);
    } 

  },
  pkg: require('../../../package.json'),
  name: 'user_routes_v1'
};