'use strict';

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

// var moment = require('moment');
// moment.locale('fr');
// let FCM = require('../fcm');

/**
 * Commentaire Schema
 */
var CommentSchema = new Schema({
  texte: { type: String },
  think: { type: Mongoose.Schema.Types.ObjectId, ref: 'Think' },
  createdBy: {type: Mongoose.Schema.Types.ObjectId, ref: 'Admin' },
  updatedBy: {type: Mongoose.Schema.Types.ObjectId, ref: 'Admin' }
},
{
  timestamps: true,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

Mongoose.model('Comment', CommentSchema);