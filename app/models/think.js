'use strict';

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

/**
 * Think Schema
 */
var ThinkSchema = new Schema({
  author: { type: String },
  texte: { type: String },
  ref: { type: String },
  like: {type: Boolean, default: false},
  share: {type: Boolean, default: false},
  favorite: {type: Boolean, default: false},
  nbrcom: {type : Number, default: 0},
  createdBy: {type: Mongoose.Schema.Types.ObjectId, ref: 'Admin' },
  updatedBy: {type: Mongoose.Schema.Types.ObjectId, ref: 'Admin' }

},
{
  timestamps: true,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});


Mongoose.model('Think', ThinkSchema);