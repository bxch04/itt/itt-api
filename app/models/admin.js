'use strict';

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const crypto = require('crypto');
const { string } = require('joi');

/**
 * Admin Schema
 */
var AdminSchema = new Schema({
  email: { type: String, unique: true },
  password: { type: String },
  firstname: { type: String },
  lastname: { type: String },
  pseudo: { type: String, unique: true },
  role: { 
    type: String, 
    enum: ['admin','user'], 
    default: 'user'
  },
},
{
  timestamps: true,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * Hook a pre save method to hash the password
 */
AdminSchema.pre('save', function (next) {
  if (this.password && this.isModified('password')) {
    this.password = this.hashPassword(this.password);
  }
  next();
});

/**
 * Create instance method for hashing a password
 */
AdminSchema.methods.hashPassword = function (password) {
  var shaSum = crypto.createHash('sha256');
  shaSum.update(password);
  return shaSum.digest('hex');
};

/**
 * Create instance method for authenticating Admin
 */
AdminSchema.methods.authenticate = function (password) {
  return this.password === this.hashPassword(password);
};

Mongoose.model('Admin', AdminSchema);