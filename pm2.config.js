module.exports = {
  apps: [{
    name: 'skooladmin-api-main-api',
    script: 'server.js',
    instances: "max",
    exec_mode: 'cluster',
    max_memory_restart: '3000M',
    restart_delay: 3000,
    error_file: 'logs/err.log',
    out_file: 'logs/out.log',
    log_file: 'logs/combined.log',
  }]
}